import Vue from 'vue'
import App from './App.vue'
import VueCarousel from 'vue-carousel'

import './assets/css/style.css';

Vue.use(VueCarousel);

new Vue({
  el: '#app',
  render: h => h(App)
})
